package ad1.ss17.pa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Alexander on 08.05.2017.
 */
public class FastTest {

    public static void main(String[] args) {
        //Testing for fucking öäüÜÖÄß
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("src/ad1/ss17/pa/Graph.java")));
            String line;
            int i = 1;
            while ((line = br.readLine()) != null) {
                if(line.contains("ö")) System.err.println("ö at Line "+i);
                if(line.contains("ä")) System.err.println("ä at Line "+i);
                if(line.contains("ü")) System.err.println("ü at Line "+i);
                if(line.contains("Ü")) System.err.println("Ü at Line "+i);
                if(line.contains("Ä")) System.err.println("Ä at Line "+i);
                if(line.contains("Ö")) System.err.println("Ö at Line "+i);
                if(line.contains("ß")) System.err.println("ß at Line "+i);
                i++;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        long firstStartTime = System.nanoTime();
        for(String s:args) {
            long startTime = System.nanoTime();
            Tester.main(new String[]{s});
            long endTime = System.nanoTime();
            System.out.println(s+": "+((endTime - startTime) * 1.0 / 1000000000));
            Tester.reset();
        }
        System.out.println("Gesamtlaufzeit: "+((System.nanoTime() - firstStartTime) * 1.0 / 1000000000));
    }

}
