package ad1.ss17.pa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * This class includes the {@link #main main()}-method for the program
 * and {@link #printDebug(String)} to print debug messages
 * <p>
 * <b>IMPORTANT:</b> Do not change this class. All changes made to this class
 * are removed after the submission of the solution. Thereby, the submitted solution
 * can fail during the tests.
 * </p>
 */
public class Tester {
    /**
     * Name of file with test instances. Is <code>
     * null</code>, if read from {@link System#in}.
     */
    private static String fileName = null;

    /**
     * Adapted path
     */
    private static String choppedFileName;

    /**
     * Test flag for output during test run
     */
    private static boolean test = false;

    /**
     * Debug flag for additional debug messages
     */
    private static boolean debug = false;

    /**
     * Prints message <code>msg</code> and exits program.
     *
     * @param msg the printed message.
     */
    private static void bailOut(String msg) {
        System.out.println();
        System.err.println((test ? choppedFileName + ": " : "") + "ERR " + msg);
        System.exit(1);
    }

    /**
     * Generates a chopped String representation of the file name.
     */
    private static void chopFileName() {
        if (fileName == null) {
            choppedFileName = "System.in";
            return;
        }
        int i = fileName.lastIndexOf(File.separatorChar);
        if (i > 0) {
            i = fileName.lastIndexOf(File.separatorChar, i - 1);
        }
        if (i == -1) {
            i = 0;
        }
        choppedFileName = ((i > 0) ? "..." : "") + fileName.substring(i);
    }

    /**
     * Prints a debugging message. If the program is started with <code>-d</code>
     * a message <code>msg</code> and the file name of the test instance
     * is printed. Otherwise nothing is printed.
     *
     * @param msg output text.
     */
    public static void printDebug(String msg) {
        if (!debug) {
            return;
        }
        System.out.println(choppedFileName + ": DBG " + msg);
    }

    /**
     * Opens the input file and returns an object of type {@link Scanner} for reading from the file.
     * If no file name is given, input is read from {@link System#in}.
     *
     * @return {@link Scanner} reading from input file.
     */
    private static Scanner openInputFile() {
        if (fileName != null) {
            try {
                return new Scanner(new File(fileName));
            } catch (Exception e) {
                bailOut("could not open \"" + fileName + "\" for reading");
            }
        }
        return new Scanner(System.in);
    }

    /**
     * Interprets parameters for the program  and returns
     * a {@link Scanner} reading from test file.
     *
     * @param args command line parameters
     * @return {@link Scanner} reading from test file.
     */

    private static Scanner processArgs(String[] args) {
        for (String a : args) {
            if (a.equals("-t")) {
                test = true;
            } else if (a.equals("-d")) {
                debug = test = true;
            } else if (fileName == null) {
                fileName = a;
            }
        }
        return openInputFile();
    }

    /**
     * The constructor is private to hide it from JavaDoc.
     */
    private Tester() {
    }


    /**
     * Checks structure of graph
     *
     * @param graph      graph to check
     * @param parts      number of nodes and edges
     * @param lineNumber line number in test file
     */
    private static void checkStructure(Graph graph, String[] parts, int lineNumber) {
        long startTime = System.nanoTime();
        int expected, calculated;
        expected = Integer.valueOf(parts[0]);
        calculated = graph.numberOfNodes();
        if (expected != calculated) {
            bailOut("Error in input file at line number " + lineNumber + ": Number of nodes is not correct (" + expected + " expected, " + calculated + " calculated)!");
        }
        expected = Integer.valueOf(parts[1]);
        calculated = graph.numberOfEdges();
        if (expected != calculated) {
            bailOut("Error in input file at line number " + lineNumber + ": Number of connections is not correct (" + expected + " expected, " + calculated + " calculated)!");
        }
        printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds for structure check");
    }

    /**
     * Checks reachable nodes
     *
     * @param graph      graph to check
     * @param parts      reachable nodes
     * @param lineNumber line number in test file
     */
    private static void checkReachable(Graph graph, String[] parts, int lineNumber) {
        long startTime = System.nanoTime();
        int expected, counter = 1;
        Iterable<Integer> nodes = graph.getReachableNodes(Integer.parseInt(parts[0]));
        int returnsize = 0;
        if (nodes instanceof Collection) {
            returnsize = ((Collection<?>) nodes).size();
        } else {
            int count = 0;
            Iterator iterator = nodes.iterator();
            while (iterator.hasNext()) {
                iterator.next();
                count++;
            }
            returnsize = count;
        }
        if (returnsize != parts.length - 1) {
            bailOut("Error in input file at line number " + lineNumber + ": Wrong number of reachable nodes("+Integer.parseInt(parts[0])+"). Expected="+(parts.length - 1)+" Result="+returnsize);
        } else {
            for (int calculated : nodes) {
                expected = Integer.valueOf(parts[counter++]);
                if (expected != calculated) {
                    bailOut("Error in input file at line number " + lineNumber + ": Reachable node " + expected + " expected but node " + calculated + " found");
                }
            }
        }
        printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds for reachable nodes check");
    }

    /**
     * Checks if graph is a DAG
     *
     * @param graph      graph to check
     * @param parts      expected boolean value
     * @param lineNumber line number in test file
     */
    private static void checkDAG(Graph graph, String[] parts, int lineNumber) {
        long startTime = System.nanoTime();
        boolean expected = Boolean.parseBoolean(parts[0]);
        boolean calculated = graph.isDAG();
        if (calculated != expected) {
            bailOut("Error in input file at line number " + lineNumber + ": DAG check failed (" + expected + " expected, " + calculated + " calculated)!");
        }
        printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds for DAG check");
    }


    /**
     * Checks rank of a node in the topological order
     *
     * @param graph      graph to check
     * @param parts      node and rank information
     * @param lineNumber line number in test file
     */
    private static void checkRank(Graph graph, String[] parts, int lineNumber) {
        long startTime = System.nanoTime();
        int expected, calculated;
        expected = Integer.parseInt(parts[1]);
        calculated = graph.rankInOrder(Integer.parseInt(parts[0]));
        if (calculated != expected) {
            bailOut("Error in input file at line number " + lineNumber + ": Rank " + expected + " expected but " + calculated + " calculated");
        }

        printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds for rank check");
    }


    /**
     * Checks if the graph is strongly connected
     *
     * @param graph      graph to check
     * @param parts      expected boolean value
     * @param lineNumber line number in test file
     */
    private static void checkConnected(Graph graph, String[] parts, int lineNumber) {
        long startTime = System.nanoTime();
        boolean expected = Boolean.parseBoolean(parts[0]);
        boolean connected = graph.isStronglyConnected();
        if (connected != expected) {
            bailOut("Error in input file at line number " + lineNumber + ": Connection check failed (" + expected + " expected, " + connected + " calculated)!");
        }
        printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds for connection check");
    }

    /**
     * Checks if two nodes are strongly connected
     *
     * @param graph      graph to check
     * @param parts      two nodes
     * @param lineNumber line number in test file
     */
    private static void checkComponent(Graph graph, String[] parts, int lineNumber) {
        long startTime = System.nanoTime();
        boolean expected, calculated;
        expected = Boolean.parseBoolean(parts[2]);
        calculated = graph.checkComponent(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]));
        if (calculated != expected) {
            bailOut("Error in input file at line number " + lineNumber + ": Component check failed (" + expected + " expected but " + calculated + " calculated)!");
        }

        printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds for component check");
    }


    public static void main(String[] args) {
        //System.out.println("Working Directory = " +  System.getProperty("user.dir"));
        Scanner s = processArgs(args);
        chopFileName();
        try {
            SecurityManager sm = new ADS1SecurityManager();
            System.setSecurityManager(sm);
        } catch (SecurityException e) {
            bailOut("Error: could not set security manager: " + e);
        }
        try {
            Graph network = null;
            boolean growing = true;
            boolean check = false;
            String help, first;
            String[] parts;
            int node, lineNumber = 1;
            long startTime = System.nanoTime();
            while (s.hasNextLine()) {
                help = s.nextLine();
                if (help.length() == 0) {
                    continue;
                }
                parts = help.toLowerCase().split("\\s+");
                first = parts[0];
                parts = Arrays.copyOfRange(parts, 1, parts.length);
                if (lineNumber == 1) {
                    if (first.equals("nodes")) {
                        network = new Graph(Integer.valueOf(parts[0]));
                    } else {
                        bailOut("Error in input file at line number " + lineNumber + ": Check syntax of first line!");
                    }
                } else {
                    switch (first) {
                        case "structure":
                            checkStructure(network, parts, lineNumber);
                            break;
                        case "reachable":
                            checkReachable(network, parts, lineNumber);
                            break;
                        case "dag":
                            checkDAG(network, parts, lineNumber);
                            break;
                        case "rank":
                            checkRank(network, parts, lineNumber);
                            break;
                        case "connected":
                            checkConnected(network, parts, lineNumber);
                            break;
                        case "component":
                            checkComponent(network, parts, lineNumber);
                            break;
                        case "add":
                            growing = true;
                            break;
                        case "delete":
                            growing = false;
                            break;
                        default:
                            node = Integer.valueOf(first);
                            if (parts.length == 0) {
                                if (growing) {
                                    network.addAllEdges(node);
                                } else {
                                    network.deleteAllEdges(node);
                                }
                            } else {
                                for (int i = 0; i < parts.length; i++) {
                                    if (growing) {
                                        network.addEdge(node, Integer.valueOf(parts[i]));
                                    } else {
                                        network.deleteEdge(node, Integer.valueOf(parts[i]));
                                    }
                                }
                            }
                    }
                }
                lineNumber++;
            }
            printDebug((System.nanoTime() - startTime) * 1.0 / 1000000000 + " seconds to complete whole test");
            StringBuffer msg = new StringBuffer(test ? choppedFileName + ": " : "");
            msg.append("OK");
            System.out.println("");
            System.out.println(msg.toString());
        } catch (SecurityException se) {
            bailOut("Method call not allowed: \"" + se.toString() + "\"");
        } catch (NumberFormatException e) {
            bailOut("Wrong input format: \"" + e.toString() + "\"");
        } catch (Exception e) {
            e.printStackTrace();
            bailOut("Caught exception \"" + e.toString() + "\"");
        }
    }

    public static void reset(){
        fileName = null;
        choppedFileName = null;
        test = false;
        debug = false;

    }
}
