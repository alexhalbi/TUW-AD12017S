package ad1.ss17.pa;

import java.util.*;
/**
 * Alexander Halbarth 1129193 alexander.halbarth@student.tuwien.ac.at
 */
public class Graph {
    private ArrayList<TreeSet<Integer>> nodes;  //ArrayList verwaltung eines Arrays
                                                //TreeSet Baum mit automatischer Sortierung
                                                    //Theta(log(n)) fuer add, remove und contains
                                                    //automatisch aufsteigend sortiert fuer int
    private int edges;      //Anzahl der Kanten
    private byte dag;       // -1 noch kein Aufruf, 0 false, 1 true
    private byte strong;    // -1 noch kein Aufruf, 0 false, 1 true

    public Graph(int n) {
        nodes = new ArrayList<>(n);
        for(int i=0;i<n;i++)
            nodes.add(i,new TreeSet<>());
        edges = 0;
        dag=-1;
        strong=-1;
    }

    /**
     * @return Anzahl der Knoten
     */
    public int numberOfNodes() {
        return nodes.size();
    }

    /**
     * @return Anzahl der Kanten
     */
    public int numberOfEdges() {
        return edges;
    }

    /**
     * Fuegt eine Kante vom Knoten v zum Knoten w ein. Ist diese Kante schon vorhanden, dann passiert nichts,
     * d.h. die Kante bleibt im Graph erhalten.
     * @param v Knoten
     * @param w Kante von v zu w
     */
    public void addEdge(int v, int w) {
        Tester.printDebug("Add Edge: "+v+' '+w);
        if(v==w)
            throw new IllegalArgumentException("Keine Schleifen!");
        edges++;
        dag=-1; //Kreis koennte entstanden sein!
        if(strong==0) strong=-1; //stark Zusammenhaengend kann nur verbessert werden!
        nodes.get(v).add(w);
    }

    /**
     * Fuegt Kanten von einem bestimmten Knoten v zu allen anderen Knoten ein. Fuehren von diesem Knoten schon Kanten
     * weg, dann bleiben diese erhalten.
     * @param v Knoten
     */
    public void addAllEdges(int v) {
        for(int i = 0; i < numberOfNodes();i++) {
            if(i==v) continue;
            addEdge(v,i);
        }
    }

    /**
     * Entfernt die Kante vom Knoten v zum Knoten w. Ist die Kante nicht vorhanden, dann passiert nichts.
     * @param v Knoten
     * @param w Kante von v zu w loeschen
     */
    public void deleteEdge(int v, int w) {
        edges--;
        strong=-1;//stark Zusammenhaengend nicht mehr garantiert!
        if(dag==0) dag=-1; //dag kann nur verbessert werden!
        nodes.get(v).remove(w);
    }

    /**
     * Entfernt fuer einen bestimmten Knoten v alle Kanten zu seinen adjazenten Knoten.
     * Fuehren von diesem Knoten noch keine Kanten weg, dann passiert nichts.
     * @param v Knoten
     */
    public void deleteAllEdges(int v) {
        edges -= nodes.get(v).size();
        nodes.set(v, new TreeSet<>());
        if(dag==0) dag=-1; //dag kann nur verbessert werden!
        strong=0; //auf jeden Fall nicht mehr strongly connected, da Knoten v keinen anderen erreichen kann!
    }

    /**
     * Liefert alle Knoten, die vom Knoten v aus erreicht werden koennen, in einer Datenstruktur zurueck, ueber
     * die iteriert werden kann. Die Knoten sollen ohne v in aufsteigender Reihenfolge zurueckgeliefert werden.
     * @param v Knoten
     * @return Alle erreichbaren Knoten als Iterable
     */
    public Iterable<Integer> getReachableNodes(int v) {
        return getReachableNodesTree(v);
    }

    /**
     * Wie getReachableNodes(int v), aber gibt kein Iterable aus, sondern ein TreeSet.
     *
     * Benoetigt fuer boolean checkComponent(int i, int j)
     * @param v Knoten
     * @return Alle erreichbaren Knoten als TreeSet
     */
    public TreeSet<Integer> getReachableNodesTree(int v) {
        TreeSet<Integer> solution =  getReachableNodesTreeDAG(v);
        solution.remove(v);
        return solution;
    }

    /**
     * Wie getReachableNodesTree(int v), aber der aufrufende Knoten wird nicht entfernt (Kreise finden!).
     *
     * Benoetigt fuer boolean isDAG()
     * @param v Knoten
     * @return Alle erreichbaren Knoten ohne beruecksichtigung von Kreisen
     */
    public TreeSet<Integer> getReachableNodesTreeDAG(int v) {
        TreeSet<Integer> nv = nodes.get(v);
        TreeSet<Integer> solution =  new TreeSet<>(nv);
        boolean[] expl =  new boolean[numberOfNodes()];
        LinkedList<Integer> to_expl = new LinkedList<>(nv);
        expl[v]=true;
        Tester.printDebug("getReachableNodes: "+v+' '+nv);
        while (to_expl.peekFirst() != null) {
            int n = to_expl.pollFirst();
            if(!expl[n]) {
                solution.addAll(nodes.get(n));
                to_expl.addAll(nodes.get(n));
                expl[n]=true;
            }
        }
        //solution.remove(v);
        Tester.printDebug("getReachableNodes: "+v+" Solution="+solution);
        return solution;
    }


    /**
     * Ueberprueft, ob der gesamte Graph ein gerichteter azyklischer Graph (Directed Acyclic Graph, DAG) ist.
     * Wenn dies der Fall ist, wird true zurueckgeliefert, ansonsten false.
     * @return ist ein gerichteter azyklischer Graph (Directed Acyclic Graph, DAG)
     */
    public boolean isDAG() {
        if(dag==1||numberOfEdges()==0||numberOfNodes()==1) return true;
        else if (dag==0) return false;
        else {
            for (int i = 0; i < numberOfNodes(); i++) {
                if (getReachableNodesTreeDAG(i).contains(i)) {
                    dag=0;
                    return false;
                }
            }
            dag=1;
            return true;
        }
    }

    /**
     * Liefert fuer Knoten v den Rang in der topologischen Sortierreihenfolge zurueck.
     * Der erste Knoten in der Sortierreihenfolge hat Rang 0, der letzte Rang n - 1. Muss zwischen mehreren Knoten
     * gewaehlt werden (da sie alle den Eingangsgrad 0 haben), dann wird immer der Knoten mit dem hoechsten Index
     * gewaehlt. Ist eine topologische Sortierung nicht moeglich, dann haben alle Knoten den Rang -1.
     *
     * Implementieren Sie die topologische Sortierung wie in der Vorlesung beschrieben. Dabei wird ein neuer
     * Knoten immer am Anfang der Liste hinzugefuegt. Der Algorithmus startet zunaechst bei dem Knoten mit dem
     * niedrigsten Indexwert aus der Menge der Knoten mit Eingangsgrad 0. Stellen Sie sicher, dass die adjazenten
     * Knoten eines Knotens immer in aufsteigender Reihenfolge abgespeichert werden und daher auch immer in
     * aufsteigender Reihenfolge bearbeitet werden. In unseren Testinstanzen werden die adjazenten Knoten immer
     * aufsteigend angegeben.
     *
     * @param v Knoten v
     * @return Rang in der toplogischen Sortierung
     */
    public int rankInOrder(int v) {
        if(!isDAG())
            return -1;
        if(numberOfEdges()==0)
            return numberOfNodes()-v-1;
        int[] cnt = new int[numberOfNodes()];
        int rank = 0;
        //for(int i = 0; cnt.length>i;i++) //Nicht notwendig
        //    cnt[i]=0;
        for(TreeSet<Integer> t:nodes) {
            for(int i:t) {
               cnt[i]=cnt[i]+1;
            }
        }
        LinkedList<Integer> iterable = new LinkedList<>();
        for(int i = 0; cnt.length>i;i++)
            if(cnt[i]==0)
                iterable.addFirst(i);
        while(iterable.peekFirst()!=null) {
            int n = iterable.pollFirst();
            if(v==n) {
                return rank;
            }
            rank++;
            for(int i:nodes.get(n)) {
                cnt[i]=cnt[i]-1;
                if(cnt[i]==0)
                    iterable.addFirst(i);
            }
        }
        return rank;
    }


    /**
     * Ueberprueft, ob der gesamte Graph stark zusammenhaengend ist. Wenn dies der Fall ist,
     * wird true zurueckgeliefert, ansonsten false.
     * @return gesamter Graph stark zusammenhaengend
     */
    public boolean isStronglyConnected() {
        if(strong==1||numberOfNodes()==1) return true;  //Graph mit 1 Knoten ist DAG und strongly connected!
        else if(strong==0||dag==1||numberOfNodes()>numberOfEdges()) return false; //DAG sonst nie strongly connected & |V|>|E| -> not strongly connected
        else {
            //fast check:
            for(TreeSet t:nodes) {
                if(t.size()==0) {
                    strong=0;
                    return false;
                }
            }
            for(int i=0;i<numberOfNodes();i++) {
                boolean[] disc = new boolean[numberOfNodes()];
                DFS(disc,i);
                for(boolean b:disc) {
                    if(!b) {
                        strong=0;
                        return false;
                    }
                }
            }
            strong=1;
            return true;
        }
    }

    private boolean[] DFS(boolean[] disc, int u) {
        disc[u] = true;
        for(int v:nodes.get(u)) {
            if(!disc[v])
                DFS(disc, v);
        }
        return disc;
    }

    /**
     * Ueberprueft, ob sich die beiden Knoten v und w in der gleichen stark zusammenhaengenden Komponente befinden.
     * Wenn dies der Fall ist, wird true zurueckgeliefert, ansonsten false.
     *
     * In der Vorlesung wurde der starke Zusammenhang nur auf dem gesamten Graphen besprochen. In diesem Fall
     * muss man das aber auch auf Teilgraphen ueberpruefen. Z.B. kann ein Teilgraph stark zusammenhaengend sein,
     * der gesamte Graph hingegen ist es nicht.
     * @param i Knoten v
     * @param j Knoten w
     * @return Ob die beiden Knoten stark zusammenhaengend sind
     */
    public boolean checkComponent(int i, int j) {
        return getReachableNodesTree(i).contains(j) && getReachableNodesTree(j).contains(i);
    }


    public static void main(String[] args) {

    }
}



